# Instant Pot Sushi Rice

![](./resources/InstantPotSushiRice.jpg)

## Details

**Cook Time**: 22 minutes
**Prep Time**: 5 minutes
**Calories:** 123kcal
**Servings**: 12
**Source**: [https://thefoodieeats.com/instant-pot-sushi-rice-recipe/](https://thefoodieeats.com/instant-pot-sushi-rice-recipe/)

## Ingredients

##### Rice

- **2 cups** short-grain Japanese rice
- **2 cups** water

##### Rice Seasoning
- **¼ cup** white wine vinegar
- **¼ cup** seasoned rice vinegar
- **1 ½ teaspoon.** salt
- **2 tablespoons** sugar

## Instructions

##### Rice Seasoning
1. Combine white wine vinegar, rice wine vinegar, salt, and sugar in a small bowl  
2. Microwave for 1 minute, then stir until completely dissolved

##### Rice
1. In a fine mesh strainer, rinse rice under running water for about 2 minutes
2. Let rice dry for 5~10 minutes
3. Add **2 cups** of rice and water to Instant Pot
4. Use the rice setting on low pressure for 12 minutes
    - Once cook time is complete, allow pressure to naturally release (about 10 minutes)
5. Transfer rice to glass dish. Then using a wooden spoon, add vinegar mixture and gently mix well to combine
6. Let cool completely