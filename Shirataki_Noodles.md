# Shirataki Noodles

![](./resources/ShiratakiNoodles.png)

## Details

**Cook Time**: 15 minutes
**Prep Time**: 5 minutes
**Calories:** 299 Calories
**Servings**: 1

## Ingredients

### Dish
- 200g shirataki
- 150g chicken breast
- 1 medium onion (50g)
- 1 Carrot or Kohlrabi (optional)

### Sauce
- 10g gochujang
- 10g mirin
- 5-10 rice vinegar
- salt, pepper
- 2 cloves garlic
- 1 piece ginger (optional)
- 2g sesame oil
- 10-20g water

## Instructions

### Prep

- Wash and rinse noodles, and let them sit in sink for a few minutes
- Dice onion into bite-size chunks
- Cut chicken brests into bite-size chunks

### Sauce

- In a small bowl mix the following:
  - 1 or 2 teaspoons of gochujang
  - 10g mirin
  - 10g rice vinegar
  - 2 cloves of carlic
  - 2g seasame oil
  - salt and pepper to taste
  - ginger (optional)
  - 10 ~ 20g water (depending on spice level wanted)


### Cook

- Heat non-stick pan to medium-high heat
- Add chicken to dry pan (no oil), stir briefly
- Add salt and pepper
- Cover with lid and cook for around 90 seconds
- Add onions, and stir fry for 1~2 minutes until onions are cooked
- Remove chicken and onions from pan
- Lower heat to medium
- Add noodles (no oil)
- Stir until liquid is completely evaporated and noodles are slightly crispy
- Add back chicken and onions and combine
- Add sauce and mix until sauce is well distributed