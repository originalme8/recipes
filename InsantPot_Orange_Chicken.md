
# InstantPot Orange Chicken  
![](./resources/Instant-Pot-Orange-Chicken.jpg)

## Details

**Cook Time**: 15 minutes
**Prep Time**: 15 minutes
**Calories:** 337kcal
**Servings**: 6


## Ingredients

### Chicken
- **2 lbs** Chicken Breast or Thighs
- **2 Tablespoons** Vegetable Oil

### Sauce

- **1 Cup** Orange Juice (no sugar added)
- **1 Tablespoon** Ginger (grated)
- **6 Cloves** Garlic (minced)
- **1 Tablespoon** Rice Wine Vinegar (or dry white wine**)
- **1/4 Cup** Granulated Sugar
- **1/4 Cup** Brown Sugar
- **1/4 Cup** Lite Soy Sauce
- Zest from **1** Orange
- **Optional**
    - **1/2 Cup** Tomato Sauce
    - **1 Tablespoon** Sriracha

### Slurry
- **2 Tablespoons** Cornstarch
- **2 Tablespoons** Orange Juice

### Garnish
- **4** Green Onions (sliced)
- Extra Orange Zest

## Instructions

### Prep

##### Chicken

1. Dry chicken as much as possible with towels (or paper towels)
2. **Cut** chicken into **1-2 inch chunks**

##### Slurry
1. In a medium bowl combine the following:
    - 2 Tablespoons of Cornstarch 
    - 2 Tablespoons of Orange Juice
2. Whisk until all combined with no lumps

##### Garnish

### Cook

1. Heat a large pan over medium-high heat
2. Once pan is to temperature, **add** the **peanut oil** to the pan
3. **Saute** the cut up chicken until **golden-brown** (**2-3 minutes**)
4. Move the chicken to the instant pot
5. Add the sauce ingredients to the pot
    - 1 Cups of Orange Juice
    - Garlic
    - Ginger
    - Soy Sauce
    - White Sugar
    - Brown Sugar
    - Rice Wine Vinegar
    - Orange Zest
    - Sriracha sauce
      - *You can skip the Sriracha sauce or add more if you prefer your food on the spicier side*
    - Add the tomato sauce if you are using it. 
        - *The tomato sauce adds a tanginess to the overall sweet recipe, and it makes it taste more savory*
6. Set the Instant Pot to pressure cook on High for 5 minutes
7. Use a 10-minute Natural Release
8. Turn off the heat and **release the remaining pressure** via the quick release valve
9. Set the InstantPot to **Saute** on **low**
10. Add the mixture to the Instant Pot and gently stir to combine
11. Bring mixture to **low simmer**
12. **Simmer for 2-3 minutes**
    - *If you want the sauce even thicker, mix one more tablespoon of cornstarch with orange juice and add it to the pot*
13. Let the Orange Chicken **stand for 5-7 minutes** 
    - *The sauce will thicken more*

### Serve

1. Serve over rice and garnish with fresh chopped green onions and extra orange zest