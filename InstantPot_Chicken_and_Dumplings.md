# Instant Pot Chicken and Dumplings

![](./resources/InstantPotChickenandDumplings.jpg)

## Details

**Cook Time**: 15 minutes
**Prep Time**: 15 minutes
**Calories:** 337kcal
**Servings**: 5
**Source**: [https://www.pillsbury.com/recipes/instant-pot-chicken-and-dumplings/4dd9c45e-4317-438c-9432-d4baafdeaac4](https://www.pillsbury.com/recipes/instant-pot-chicken-and-dumplings/4dd9c45e-4317-438c-9432-d4baafdeaac4)


## Ingredients

#### Requirements
- 6qt or larger Instant Pot

#### Ingrediants

##### Poultry Seasoning (If not using pre-mix)
- 2 tsp ground dried sage
- 1 1/2 teaspoons ground dried thyme
- 1 teaspoon ground dried marjoram
- 3/4 teaspoon ground dried rosemary
- 1/2 teaspoon ground nutmeg
- 1/2 teaspoon finely ground black pepper 

##### Main
- 2  tablespoons butter 
- 2  cups chopped yellow onions
- 1  teaspoon poultry seasoning 
- 1/2  teaspoon salt 
- 1  cup chopped celery
- 1  cup chopped peeled carrots 
- 2  cups  chicken broth 
- 1  lb boneless skinless chicken breasts, cut in 1-inch pieces 
- 1  can (10.2 oz) refrigerated *Pillsbury Grands! Southern Home style Buttermilk Biscuits* (5 Count)
- 1/2  cup heavy whipping cream 

## Instructions

#### Prep

##### Meat & Veggies

- Cube **chicken breasts** into 1" pieces
- Peel and chop **carrots** into 1/4" rounds
- Chop **onions** into 1/4" pieces
- Separate **biscuits** and then **chop** separated pieces into 6~8 pieces

##### Poultry Seasoning

1. In a bowl, combine the ingredients below:
  - 2 tsp ground dried sage
  -  1 ½ teaspoons ground dried thyme
  - 1 teaspoon ground dried marjoram
  - 3/4 teaspoon ground dried rosemary
  - 1/2 teaspoon ground nutmeg
  - 1/2 teaspoon finely ground black pepper 

#### Cook

1. Set the InstantPot to Sauté on **Medium**
2. **Melt butter** into InstantPot
3. Add the following items and cook for __6 to 8 minutes__ (stirring occasionally)
    - Onions
    - Poultry Seasoning
    - Salt
4. Once onions are soft, **turn off** the InstantPot
5. Stir in the **celery**, **carrots**, **broth**, and **chicken**
6. Seal the InstantPot and set it to **pressure-cook** on **high** for *2 minutes*
    - Quick release when done
7. Set InstantPot to **sauté on low**
8. Stir in **whipping cream** and bring to simmer
9. Add **biscuits** and sauté for 4~6 minutes (until biscuits are cooked and liquid is thickened)